package Main;

import java.io.Console;

import Data.CDRRepository;
import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Data.PhoneNumberRepository;
import Entities.CDR;
import Entities.Phone_Number;
import Persistencia.FileTxt;
import Persistencia.IDataPersistence;

public class main {
	public static void main(String[] args) {
		
		IPhoneNumberRepository PhonesRepository = new PhoneNumberRepository();
		ICDRRepository CDRsRepository = new CDRRepository();
		IDataPersistence DataPersistence = new FileTxt();
		Call_Center x = new Call_Center();
		//Phones
		Phone_Number phone_prepago = new Phone_Number(1111,20,"PLAN_PREPAGO");
		Phone_Number phone_wow = new Phone_Number(2222,20,"PLAN_WOW");
		Phone_Number phone_postpago = new Phone_Number(3333,20,"PLAN_POSTPAGO");
		Phone_Number phone_wow_2 = new Phone_Number(4444,20,"PLAN_WOW");
		Phone_Number phone_postpag_2 = new Phone_Number(5555,20,"PLAN_POSTPAGO");
		Phone_Number phone_prepago_2 = new Phone_Number(6666,20,"PLAN_PREPAGO");
		
		//Registering Friends numbers
		phone_wow.registerFriendNumber(1);
		phone_wow.registerFriendNumber(2);
		phone_wow.registerFriendNumber(3);

		phone_wow_2.registerFriendNumber(1111);
		phone_wow_2.registerFriendNumber(2222);
		phone_wow_2.registerFriendNumber(3333);
		//Registering phones		
		PhonesRepository.RegisterNewNumber(phone_prepago);
		PhonesRepository.RegisterNewNumber(phone_wow);
		PhonesRepository.RegisterNewNumber(phone_postpago);
		PhonesRepository.RegisterNewNumber(phone_wow_2);
		PhonesRepository.RegisterNewNumber(phone_postpag_2);
		PhonesRepository.RegisterNewNumber(phone_prepago_2);
		
		//CDR /originNumber/destinationNumber/durationCall/date/hour) 
		
		CDR register1 = new CDR(1111,1,"00:02:45", "DATE", "01:02:45");
		CDR register5 = new CDR(1111,123,"00:02:00", "DATE", "02:02:45");
		CDR register2 = new CDR(2222,1,"00:04:00", "DATE", "16:02:45");
		CDR register4 = new CDR(2222,123,"00:00:45", "DATE", "12:02:45");
		CDR register3 = new CDR(3333,789,"00:01:45", "DATE", "16:02:45");
		CDR register6 = new CDR(5555,123,"00:02:50", "DATE", "17:02:45");
		CDR register8 = new CDR(4444,123,"00:04:30", "DATE", "09:02:45");
		CDR register9 = new CDR(4444,123,"00:12:20", "DATE", "07:02:45");
		CDR register10 = new CDR(6666,3,"00:15:00", "DATE", "02:02:45");
		CDR register7 = new CDR(6666,3333,"00:13:00", "DATE", "16:02:45");
		//Registering CDRs
		CDRsRepository.RegisterCDR(register1);
		CDRsRepository.RegisterCDR(register2);
		CDRsRepository.RegisterCDR(register3);
		CDRsRepository.RegisterCDR(register4);
		CDRsRepository.RegisterCDR(register5);
		CDRsRepository.RegisterCDR(register6);
		CDRsRepository.RegisterCDR(register8);
		CDRsRepository.RegisterCDR(register10);
		CDRsRepository.RegisterCDR(register9);
		CDRsRepository.RegisterCDR(register7);
		
		CallCenterService CCService = new CallCenterService(CDRsRepository, PhonesRepository, DataPersistence);		
	

		//Show cost of all CDRs empties
		System.out.println("Before calculate costs for all CDRs");
		CCService.showDataCDR();
		
		//Calculate CDRs cost
		CCService.CalculateCostCDRs();

		//Show cost of all CDRs
		System.out.println("After calculate costs for all CDRs");
		CCService.showDataCDR();
		
		//Serialization of classes
		CCService.ExportData();
	}
}
