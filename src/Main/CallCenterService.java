package Main;

import java.util.List;

import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Entities.CDR;
import Entities.Phone_Number;
import Persistencia.IDataPersistence;

public class CallCenterService  {
	
	ICDRRepository CDRs;
	IPhoneNumberRepository PhoneNumbers;
	IDataPersistence DataPersistence;
	public CallCenterService(ICDRRepository CDRs,IPhoneNumberRepository PhoneNumbers, IDataPersistence DataPersistence) {
		this.CDRs = CDRs;
		this.PhoneNumbers = PhoneNumbers;
		this.DataPersistence= DataPersistence;
	}
	
	public void registerCDR(CDR NewCDRs) {
		CDRs.RegisterCDR(NewCDRs);
	}
	
	public void registerNumber(Phone_Number NewNumber) {
		PhoneNumbers.RegisterNewNumber(NewNumber);
	}
	
	public void registerFriendNumber(int NewNumber,int FriendNumber) {
		PhoneNumbers.RegisterFriendNumber(NewNumber,FriendNumber);
	}
	
	public void CalculateCostCDRs() {
		CDRs.CalculateRateForAll(PhoneNumbers.getNumbers());
	}
	
	public List<CDR> CalculateRateForNumber(Phone_Number number){
		return CDRs.CalculateRateForOne(number);
	}
	
	public Phone_Number searchNumber(int number) {
		return PhoneNumbers.getNumber(number);
	}
	
	public void showDataCDR() {
		CDRs.ShowAllCDRs();
	}
	
	public void showDataPhoneNumber() {
		PhoneNumbers.ShowPhoneNumber();
	}
	
	public void ExportData() {
		DataPersistence.SaveData(CDRs.getCDRs());
	}
	
}
 