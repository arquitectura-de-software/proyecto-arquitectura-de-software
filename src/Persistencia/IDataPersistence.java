package Persistencia;

import java.util.List;

import Entities.CDR;

public interface IDataPersistence {
	void SaveData(List<CDR> CDRs);
	List<CDR> LoadData();
}
