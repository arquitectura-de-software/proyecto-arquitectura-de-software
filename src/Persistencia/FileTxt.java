package Persistencia;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import Entities.CDR;

public class FileTxt implements IDataPersistence {

	public void SaveData(List<CDR> CDRs) {
		try {
			FileOutputStream file = new FileOutputStream("DataSerializated.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(CDRs);
			out.close();
			file.close();
		} catch (IOException ex) {
			System.out.println("Exception caught");
		}
		
	}

	@Override
	public List<CDR> LoadData() {
		// TODO Auto-generated method stub
		return null;
	}

}
