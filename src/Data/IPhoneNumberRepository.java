package Data;

import java.util.List;

import Entities.Phone_Number;

public interface IPhoneNumberRepository {
	List<Phone_Number> getNumbers();
	void RegisterNewNumber(Phone_Number NewNumber);
	void RegisterFriendNumber(int number,int friendNumber);
	void ShowPhoneNumber();
	Phone_Number getNumber(int number);
	
	
}
