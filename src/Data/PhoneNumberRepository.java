package Data;

import java.util.ArrayList;
import java.util.List;

import Entities.Phone_Number;

public class PhoneNumberRepository implements IPhoneNumberRepository{

	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>(); ;
	@Override
	public List<Phone_Number> getNumbers() {
		return ListPhoneNumbers;
	}

	@Override
	public void RegisterNewNumber(Phone_Number NewNumber) {
		ListPhoneNumbers.add(NewNumber);
	}

	@Override
	public void RegisterFriendNumber(int number, int friendNumber) {
		Phone_Number numberDB = getNumber(number);
		if (validateNumber(numberDB)) {
			numberDB.registerFriendNumber(friendNumber);
		}
	}

	@Override
	public void ShowPhoneNumber() {
		for (Phone_Number phone_Number : ListPhoneNumbers) {
			phone_Number.showAllInformation();
		}
	}
	
	@Override
	public Phone_Number getNumber(int number) {
		for (Phone_Number phone_Number : ListPhoneNumbers) {
			if (phone_Number.getTelephone()==number) {
				return phone_Number;
			}
		}
		return null;
	}
	
	private boolean validateNumber(Phone_Number numbeDB)
	{
		if (numbeDB==null) 
			return false;
		return true;
	}
	

}
