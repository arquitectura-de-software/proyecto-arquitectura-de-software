package Data;

import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public interface ICDRRepository {
	
	List<CDR> getCDRs();
	void RegisterCDR(CDR CDR);
	void ShowAllCDRs();
	void LoadCDRs(List<CDR> CDRs);
	List<CDR> CalculateRateForOne(Phone_Number number);
	List<CDR> CalculateRateForAll(List<Phone_Number> number);
}
