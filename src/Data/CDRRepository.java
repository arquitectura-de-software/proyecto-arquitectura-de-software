package Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public class CDRRepository implements ICDRRepository{

	List<CDR> CDRs = new ArrayList<CDR>(); 
	private List<CDR> result;

	public CDRRepository() {}

	@Override
	public void LoadCDRs(List<CDR> CDRs) {
		CDRs.addAll(CDRs);
	}
	
	@Override
	public List<CDR> getCDRs() {
		return CDRs;
	}

	@Override
	public void RegisterCDR(CDR cdr) {
		CDRs.add(cdr);
	}

	@Override
	public void ShowAllCDRs() {
		for (CDR cdr : CDRs) {
			cdr.showInformation();
		}
	}

	@Override
	public List<CDR> CalculateRateForOne(Phone_Number number) {
		List<CDR> result = SearchCDRs(number);
		for (CDR cdr : result) {
			cdr.setCallCost((cdr.calculteCallCost(number)));
		}
		return result;
	}

	@Override
	public List<CDR> CalculateRateForAll(List<Phone_Number> listPhoneNumber) {
		for(CDR register : CDRs)
		{
			Phone_Number phone = searchPhone(register.getOriginNumber(),listPhoneNumber);
			register.setCallCost(register.calculteCallCost(phone));
		}
		return CDRs;
	}

	public Phone_Number searchPhone(int numberOrigin,List<Phone_Number> listPhoneNumber) {
		for(Phone_Number phone : listPhoneNumber)
		{
			if(phone.getTelephone() == numberOrigin)
				return phone;
		}
		return null;
	}
	
	
	private List<CDR> SearchCDRs(Phone_Number number){
		result = null;
		for (CDR cdr : CDRs) {
			if (cdr.getOriginNumber() ==  number.getTelephone()) {
				result.add(cdr);
			}
		}
		return result;
	}
	
	

}
