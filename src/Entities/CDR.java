package Entities;

import java.io.Serializable;

public class CDR implements Serializable{	
	private int originNumber, destinationNumber;
	private double callCost; 
	private String durationCall, date, hour;
		
	public CDR(int originNumber,int destinationNumber, String durationCall, String date, String hour) {
		this.originNumber = originNumber;
		this.destinationNumber = destinationNumber;
		this.durationCall = durationCall;
		this.date = date;
		this.hour = hour;
	}
	
	public void setCallCost(double callCost) {
		this.callCost = callCost;
	}
	
	public int getOriginNumber() {
		return originNumber;
	}
	
	public int getDestinationNumber() {
		return destinationNumber;
	}
	
	public double getCallCost() {
		return callCost;
	}
	
	public double calculteCallCost(Phone_Number phone) {
		return phone.calculateCost(hour, durationCall, destinationNumber);
	}
	
	public void showInformation() {
		System.out.print("originNumber :: "+ originNumber);
		System.out.print(" destinationNumber :: "+ destinationNumber);
		System.out.print(" durationCall :: "+ durationCall);
		System.out.print(" date :: "+ date);
		System.out.println(" Cost :: "+ callCost);
	}
}
	
