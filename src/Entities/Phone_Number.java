package Entities;

import java.util.ArrayList;
import Constructors.Plan_Factory;
import Plans.IPlan;

public class Phone_Number {
	private static final int LIMIT_FRIENDS_NUMBERS = 4;
	private int telephone;
	private int antique;
	private double  balance;
	private IPlan plan_rate;
	private ArrayList<Integer>telephoneFriends = new ArrayList<Integer>();
	
	public Phone_Number(int telephone,double balance,String planType) {
		this.telephone = telephone;
		this.balance = balance;
		this.antique = 0;
		setPlan(planType);
	}
	
	public int getTelephone(){
		return telephone;
	}
	
	public double calculateCost(String hour,String duration,int targetTelephone){
		double result;
		result= plan_rate.calculateCost(hour, duration, targetTelephone);
		return result;
	}

	public void setPlan(String planType) {
		Plan_Factory plan = new Plan_Factory();
		plan_rate = plan.ChooseRate(planType, telephoneFriends);
	} 
	
	public String getPlan() {
		return plan_rate.getPlanType();
	}
	
	public void registerFriendNumber(int telephone) {
		if(telephoneFriends.size()<LIMIT_FRIENDS_NUMBERS);
			telephoneFriends.add(telephone);
	}
	
	public void showAllInformation() {
		System.out.print("Numero :: "+ telephone);
		System.out.print(" Saldo Restante :: " + balance);
		System.out.print(" Antiguedad :: " + antique);
		System.out.println(" Tipo de Plan ::" + plan_rate.getPlanType());
	}
}

