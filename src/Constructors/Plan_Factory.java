package Constructors;

import java.util.ArrayList;

import Plans.IPlan;
import Plans.Plan_PostPago;
import Plans.Plan_Prepago;
import Plans.Plan_Wow;

public class Plan_Factory {

	public IPlan ChooseRate(String type,ArrayList<Integer> telephoneFriends) {
		IPlan rateType = null;
		if (type == "PLAN_POSTPAGO") 
			rateType = new Plan_PostPago();
		if (type == "PLAN_PREPAGO") 
			rateType = new Plan_Prepago();
		if (type == "PLAN_WOW") 
			rateType = new Plan_Wow(telephoneFriends);
		return rateType;
	}
}
